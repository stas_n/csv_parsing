Rails.application.routes.draw do
  root 'csv#index'
  resources :csv, only: [:index] do
    get :units, on: :collection
    get :suppliers, on: :collection
  end
end
