every 1.day, at: '10.00' do
  runner 'SupplierWorker.perform_async'
end

every 1.day, at: '10.10' do
  runner 'SkuWorker.perform_async'
end