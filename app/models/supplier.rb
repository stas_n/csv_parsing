class Supplier < ApplicationRecord
  validates :code, :name, presence: true
  has_many :units, class_name: 'Sku', primary_key: 'code', foreign_key: 'supplier_code'

end
