class Sku < ApplicationRecord
  validates :sku, :supplier_code, :name, :price, presence: true
  belongs_to :supplier, primary_key: 'code', foreign_key: 'supplier_code', class_name: 'Supplier'
end