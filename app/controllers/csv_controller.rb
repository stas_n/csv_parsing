class CsvController < ApplicationController
  def index
  end

  def units
    @units = Sku.all.page(params[:page]).per(params[:per])
  end

  def suppliers
    @suppliers = Supplier.all.page(params[:page]).per(params[:per])
  end
end