class SupplierWorker
  include Sidekiq::Worker
  require 'csv'

  def perform
    Supplier.transaction do
      CSV.foreach("#{Rails.root}/csv_files/suppliers.csv", col_sep: ('¦')) do |supplier|
        Supplier.where(code: supplier[0],name: supplier[1]).first_or_create do |sup|
          sup.code = supplier[0]
          sup.name = supplier[1]
        end
      end
    end
  end
end
