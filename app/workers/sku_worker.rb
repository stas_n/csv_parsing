class SkuWorker
  include Sidekiq::Worker
  require 'csv'

  def perform
    Sku.transaction do
      CSV.foreach("#{Rails.root}/csv_files/sku.csv", col_sep: ('¦')) do |sku|
        Sku.where(sku: sku[0], supplier_code: sku[1], name: sku[2], price: sku.last).first_or_create do |unit|
          unit.sku = sku[0]
          unit.supplier_code = sku[1]
          unit.name = sku[2]
          unit.price = sku.last
        end
      end
    end
  end
end
