FactoryBot.define do
  factory :sku do
    sku Faker::Name.name
    supplier
    name Faker::NewGirl.character
    price  Faker::Number.number(4)
  end
end
