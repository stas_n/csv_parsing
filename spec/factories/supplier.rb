FactoryBot.define do
  factory :supplier do
    name Faker::Name.name
    code Faker::Number.number(4)
  end
end



