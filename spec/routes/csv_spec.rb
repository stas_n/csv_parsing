require 'spec_helper'

describe 'Routes Spec', type: :routing do
  describe 'CSV Controller' do
    it 'should have custom routes enabled' do
      expect(get: '/').to be_routable
      expect(get: '/').to route_to('csv#index')

      expect(get: '/csv/units').to be_routable
      expect(get: '/csv/units').to route_to('csv#units')

      expect(get: '/csv/suppliers').to be_routable
      expect(get: '/csv/suppliers').to route_to('csv#suppliers')
    end
  end
end