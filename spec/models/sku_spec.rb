require 'spec_helper'

describe Sku do

  context 'associations' do
    it {should belong_to(:supplier).class_name('Supplier').with_primary_key('code').
        with_foreign_key('supplier_code')}
  end

  context 'validations' do
    it {is_expected.to validate_presence_of(:sku)}
    it {is_expected.to validate_presence_of(:supplier_code)}
    it {is_expected.to validate_presence_of(:name)}
    it {is_expected.to validate_presence_of(:price)}
  end
end
