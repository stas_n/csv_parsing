require 'spec_helper'

describe Supplier do

  context 'associations' do
    it {should have_many(:units).class_name('Sku').with_primary_key('code').
        with_foreign_key('supplier_code')}
  end

  context 'validations' do
    it {is_expected.to validate_presence_of(:name)}
    it {is_expected.to validate_presence_of(:code)}
  end

end
