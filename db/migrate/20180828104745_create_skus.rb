class CreateSkus < ActiveRecord::Migration[5.1]
  def change
    create_table :skus do |t|
      t.string :sku
      t.integer :supplier_code
      t.string :name
      t.float :price

      t.timestamps
    end
  end
end
